package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.Menu;
import com.github.yulichang.base.MPJBaseMapper;

/**
 * <p>
 * 菜单管理 Mapper 接口
 * </p>
 *
 * @author Vincent
 * @since 2023-06-10
 */
public interface MenuMapper extends MPJBaseMapper<Menu> {

}
