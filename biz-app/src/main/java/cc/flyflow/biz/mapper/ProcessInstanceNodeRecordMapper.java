package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.ProcessInstanceNodeRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 流程节点记录 Mapper 接口
 * </p>
 *
 * @author xiaoge
 * @since 2023-05-10
 */
public interface ProcessInstanceNodeRecordMapper extends BaseMapper<ProcessInstanceNodeRecord> {

}
