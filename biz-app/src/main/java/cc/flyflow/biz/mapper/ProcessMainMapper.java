package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.ProcessMain;
import com.github.yulichang.base.MPJBaseMapper;

/**
 * <p>
 * 流程主表
 * </p>
 *
 * @author Vincent
 * @since 2023-06-10
 */
public interface ProcessMainMapper extends MPJBaseMapper<ProcessMain> {

}
