package cc.flyflow.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cc.flyflow.biz.entity.ProcessMain;

public interface IProcessMainService extends IService<ProcessMain> {
}
