package cc.flyflow.biz.vo;

import lombok.Data;

@Data
public class FileVO {

    private String base64;

    private String fileName;

}
