package cc.flyflow.common.dto.flow;

import lombok.Data;

@Data
public class Refuse {

    private String handler;
    private String nodeId;


}
